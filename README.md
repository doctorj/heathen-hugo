# Heathen for Hugo

A Hyperminimal Hugo Theme

## About Heathen

[Heather](//jxnblk.com/Heather/) is hyperminimal theme
for [Jekyll](//jekyllrb.com) created by Brent Jackson
([jxnblk](//jxnblk.com/)).

## About Hugo

[Hugo](//gohugo.io) is a (very) fast static site generator written in Go.

Compatible with hugo [0.15.0](http://gohugo.io/meta/release-notes/)

## Get Started

Once you have Hugo set up, create your blog with `hugo new` and then
add the heathen-hugo theme to the `themes` directory.

You can just download and extract it there or add it as a submodule
with:

    git submodule add https://gitlab.com/doctorj/heathen-hugo.git themes/heathen-hugo

Then edit your blog's config file to use heathen-hugo theme:

- `config.toml`

    ``` toml
    theme = "heathen-hugo"
    ```

- `config.yaml`

    ``` yaml
    theme: "heathen-hugo"
    ```

For example, a minimal YAML config file looks like this:

    title: "Heathen"
    baseurl: "http://localhost:1313"
    languageCode: en-us
    theme: heathen-hugo

    permalinks:
      post: /:year/:month/:title/

    taxonomies:
      tags: ["meta", "theme", "blog"]

    params:
      description: A Hyperminimal Hugo Theme
      author: 
       name: "doctorj"
    ...

---

MIT License
http://opensource.org/licenses/MIT
